/**
 * 
 */
angular.module('MyApp')

.config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
	console.log("inside route.js");
	
	//localStorageServiceProvider.setStorageType('sessionStorage');

	  /**
	   * Routes definition for navigation
	   */
      $stateProvider
      
      /**
       * Login Route
       */
        .state('login', {
          url : '/login',
          templateUrl: 'login/login.view.html'
        })
      
      /**
       ** Welocme Route
       */
      	.state('welome', {
      		url : '/welcome',
      		templateUrl: 'welcome/welcome.view.html'
      	});
      	
      	$urlRouterProvider.otherwise('/login');
}])