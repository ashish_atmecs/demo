package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.beans.Category;

@Repository
public interface CategoriesDao extends JpaRepository<Category, Integer> {

}
