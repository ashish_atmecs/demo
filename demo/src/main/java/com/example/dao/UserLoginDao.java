package com.example.dao;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.beans.UserLogin;
import java.util.List;
/**
 * Interface containing related operations to interact with
 * Database.
 * @author Ramnaresh.Mantri
 *
 */		
@Repository
public interface UserLoginDao extends JpaRepository<UserLogin, Integer>{

	@Query("FROM UserLogin ul WHERE ul.uname=?1 and ul.upass=?2")
//	@Query("FROM UserLogin ul WHERE ul.uname=?1")
	public UserLogin getUserLogin(String uname, String upass);
}
