package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.beans.Category;
import com.example.bo.CategoryBO;
import com.example.dao.CategoriesDao;

/*
 * This class contains service method which are related to database operation
 */
@Service
public class CategoriesServiceImpl implements CategoriesService{

	@Autowired
	private CategoriesDao categoriesDao;
	
	/*@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<CategoryBO> findAll()  throws Exception {
		List<Category> categoryList = categoriesDao.findAll();
		List<CategoryBO> categoryBOs = MiscUtils.createCategoryBOList(categoryList);
		return categoryBOs;
	}*/
	
	/**
	 * {@inheritDoc}
	 */
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean createCategory(CategoryBO categoryBO) throws Exception {
		try {
			System.out.println("Category name:" + categoryBO.getCategoryName() + "Category description:"
					+ categoryBO.getCategoryDescription());
			Category categories = new Category();
			categories.setCategoryName(categoryBO.getCategoryName());
			categories.setCategoryDescription(categoryBO.getCategoryDescription());
			categoriesDao.save(categories);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
