package com.example.service;
import com.example.bo.*;
public interface UserService {

	public UserResponseBo getUserLogin(String uname, String upass) throws Exception;	
}
