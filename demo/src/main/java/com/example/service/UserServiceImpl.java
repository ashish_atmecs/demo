package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.beans.UserLogin;
import com.example.bo.UserResponseBo;
import com.example.dao.UserLoginDao;
import com.example.util.MiscUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserLoginDao userLoginDao;
	
	String uname,pass;
	
	@Override	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public UserResponseBo getUserLogin(String uname, String upass) throws Exception {
		System.out.println("UserServiceImpl uname="+uname);	
		UserLogin userLogin = userLoginDao.getUserLogin(uname,upass);
		System.out.println("UserLogin Object from UserServiceImpl="+userLogin);
		UserResponseBo userResponseBO = MiscUtils.createUserResponseBO(userLogin);
		System.out.println("UserResponseBO=1 Password"+userResponseBO.getUpass());
		System.out.println("UserResponseBO=2 UserName"+userResponseBO.getUname());
		if(userResponseBO != null){
			userResponseBO.getUname();	
			
		}
		System.out.println(
				"User name: " + userResponseBO.getUname());
		
		return userResponseBO;
		
	}
}


