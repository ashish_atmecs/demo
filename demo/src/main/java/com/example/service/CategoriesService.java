package com.example.service;

import com.example.bo.CategoryBO;

public interface CategoriesService {

	/*
	 * Service method which is used to get all the categories
	 */	
	/*public List<CategoryBO> findAll() throws Exception;
	*/
	
	/*
	 * get Category by Id
	 */
	/*public CategoryBO getCategoryId(int categoryId) throws Exception;
	*/
	
	/*
	 * Method used to update category
	 */
	public boolean createCategory(CategoryBO categoryBO) throws Exception;
	

	
}
