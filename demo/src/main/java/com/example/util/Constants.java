package com.example.util;

public class Constants {
	public static final String USER_TOKEN_COOKIE_NAME = "chaperOneToken";
	// Date format patterns.
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_FORMAT_DDMMMYYYYHHMMSS = "dd MMM yyyy HH:mm:ss";
	public static final String DATE_FORMAT_MMMDDYYYYHHMMSS = "dd MMM yyyy";
	// This regex is for year format validation.
	public static final String REGEX_PATTERN_yyyyMMdd = "\\d{4}-[01]\\d-[0-9]\\d";
	public static final String UTC_TIMEZONE = "UTC";
	public static final String HYPHEN = "-";
	public static final String USER_IDENTIFICATION_STRING = "email";
	public static final String USER_SESSION_ATTRIBUTE_NAME = "USER";
	// This regex is for password validation.
	public static final String REGEX_PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,15}$";
	// Regex for email validation
	public static final String REGEX_EMAIL_PATTERN = "^[A-Z0-9._-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	public static final String RESPONSE_MESSAGE = "message";
	public static final String RESPONSE_ERROR = "error";
	public static final int COOKIE_MAX_AGE = (30 * 24 * 60 * 60);
	public static final String ROOT_PATH = "/";
	public static final String CHAPERONE_TOKEN_KEY = "chaperone_token";
	public static final String WEB_SOCKET_CLIENT_URL_PATH = "WEB_SOCKET_CLIENT_URL_PATH";
	public static final String WEB_SOCKET_CLIENT_URL = "WEB_SOCKET_CLIENT_URL";
	public static final String TOKBOX_API_KEY = "TOKBOX_API_KEY";
	public static final String USER_ATTRIBUTE_NAME = "USER";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String EMPTY_STRING = "";
	public static final String COLON = ":";
	public static final String AUTHENTICATION_HEADER = "Authorization";
	public static final String ENV_BASIC_AUTH_PASSWORD = "BASIC_AUTH_PASSWORD";
	public static final String ENV_BASIC_AUTH_USERNAME = "BASIC_AUTH_USERNAME";

	public static final int RETRY_ASYNC_WAIT_TIME = 5000;
	public static final int RETRY_ASYNC_ATTEMPTS = 5;
	public static final String ENV_APNS_PASSWORD = "APNS_PASSWORD";

//	public static final String PARTNERSPORTAL_URL = "http://stg-asdp.citrix.com";
//	public static final String PARTNERSPORTAL_AUTHORIZATION_URL="https://webapi-dev-qa.citrix.com/v1/keyservice/authenticateuserbyids?inID1=";
//	public static final String PARTNERSPORTAL_PUBLIC_IP = "10.52.111.193";
//	public static final String PARTNERSPORTAL_APIKEY = "eZpGngK6hiBHh5BMvuA8VKamGdBni3a0";
	
	public static final String PARTNERSPORTAL_PUBLIC_IP = "10.52.111.181";
	public static final String PARTNERSPORTAL_URL = "http://dev-asdp.citrix.com";
	public static final String PARTNERSPORTAL_APIKEY = "eZpGngK6hiBHh5BMvuA8VKamGdBni3a0";
	public static final String PARTNERSPORTAL_AUTHORIZATION_URL="https://webapi-dev-qa.citrix.com/v1/keyservice/authenticateuserbyids?inID1=";

	/*public static final String PARTNERSPORTAL_PUBLIC_IP = "23.29.105.193";
	public static final String PARTNERSPORTAL_URL = "https://asdp.citrix.com";
	public static final String PARTNERSPORTAL_APIKEY = "UadYxrluGNay93YfORrl4sl1LAKjGQGE";
	public static final String PARTNERSPORTAL_AUTHORIZATION_URL="https://webapi.citrix.com/v1/keyservice/authenticateuserbyids?inID1=";*/
	
	// Email configuration
	public static final String EMAIL_ADDRESS = "no-reply@citrix.com";
	public static final String DIRECT_MOTION_SAP_HEADER = "DIRECT MOTION";
	public static final String DISTI_MOTION_SAP_HEADER = "DISTI MOTION";
	public static final String BLANK_SPACE = " ";
	public static final String USD_CURRENCY = "USD";
	public static final Double REMAINING_HOURS_100_PERCENTAGE = 100.00;

	// Production Environment Constants.
	public static final String PRODUCTION_ENV = "Production";
	// Dev Environment Constants.
	public static final String DEV_ENV = "Dev";
	// Stage Environment Constants.
	public static final String STAGE_ENV = "Stage";

	// NPS Survey Link.
	public static final String NPS_SURVEY_LINK = "https://citrix.qualtrics.com/SE/?SID=";
	// NPS DEV/STAGE ID.
	public static final String NPS_SURVEY_DEV_STAGE_ID = "SV_a4ePqK4rrIkndkN";
	// NPS PRODUCTION ID.
	public static final String NPS_SURVEY_PRODUCTION_ID = "SV_dhW0kP7wlaucKu9";
	// NPS Contact ID String.
	public static final String NPS_CONTACT_ID_STRING = "contact_id";
	// NPS Project ID String.
	public static final String NPS_PROJCET_ID_STRING = "project_id";
	// NPS Partner Name String.
	public static final String NPS_PARTNER_NAME_STRING = "partner_name";
	// NPS Partner Name String.
	public static final String AND_OPERATOR_STRING = "&";
	// NPS Partner Name String.
	public static final String URL_EMPTY_STRING_REPLACER = "%20";
	// NPS Project ID String.
	public static final String EQUALS_TO_STRING = "=";
	
	//Error
	public static final int SERIVCE_UNAVAILABLE = 0;
	public static final int EMAIL_EXIST = 1;
	public static final int ROLE_NULL = 2;
	public static final int DUPLICATE_CUSTOMER_ID = 3;
	public static final int UNAUTHORIZED_USER = 4;
	
	public static final String ROLE_DISTI = "DISTI"; 
	public static final String ROLE_CITRIX_PARTNERS = "CITRIXPARTNERS"; 
	public static final String DUMMY_USER_EMAIL_EXTENSION  = "@citrix.com";
	public static final String DUMMY_USER_NAME  = "_DUMMY_USER";
	
	public static final String OPENID_CODE = "code";
	public static final String OPENID_USER = "code";
	public static final String OPENID_SCOPE = "openid email ctx_principal_aliases";
	public static final String KEYWS_URI = "https://citrixservicespreprod.citrix.com/citrixservices/key/keyws.asmx";
	
//	public static final String KEYWS_URI = "https://citrixservices.citrix.com/citrixservices/key/keyws.asmx";
}