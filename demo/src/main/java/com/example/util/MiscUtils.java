package com.example.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


import com.example.bo.CategoryBO;
import com.example.util.*;
import com.example.beans.Category;
import com.example.beans.UserLogin;
import com.example.bo.*;

/**
 * This is a utility class containing utility methods like date conversion, date
 * formatting.
 * 
 * @author TarunKumar.Singh
 * @since 2016-11-15
 *
 */
public class MiscUtils {

	
	/**
	 * This method will convert category object into category BO object.
	 * 
	 * @param category
	 *            (Category object {@link Category})
	 * @return (category BO object {@link CategoryBO})
	 */
	public static CategoryBO createCategoryBO(Category category) {
		CategoryBO categoryBO = new CategoryBO();
		categoryBO.setId(category.getId());
		categoryBO.setCategoryName(category.getCategoryName());
		categoryBO.setCategoryDescription(category.getCategoryDescription());
		return categoryBO;
	}

	/**
	 * This method will convert category list objects into category BO objects.
	 * 
	 * @param categoryList
	 *            (Category object {@link List<Category>})
	 * @return (category BO object {@link List<CategoryBO>})
	 */
	public static List<CategoryBO> createCategoryBOList(List<Category> categoryList) {
		if (categoryList == null || categoryList.size() == 0)
			return null;

		List<CategoryBO> categoryBOs = new ArrayList<CategoryBO>();
		for (Category category : categoryList) {
			if (category == null)
				continue;
			CategoryBO categoryBO = createCategoryBO(category);
			categoryBOs.add(categoryBO);
		}
		/*if (categoryBOs != null && categoryBOs.size() > 0)
			Collections.sort(categoryBOs, new SortListUtils.CategoryBOComparator());*/
		return categoryBOs;
	}

	
	
	
	/*
	 * this method will convert category list objects into category BO objects
	 * @param categoryList 
	 *                  (Category object {@link List<Category>})
	 * @ return (category BO object {@link List<CategoryBO})
	 */

	/*public static List<CategoryBO> createCategoryBOList(List<Category> categoryList) {
		if(categoryList == null || categoryList.size() == 0)
			return null;
		
		List<CategoryBO> categoryBOs = new ArrayList<CategoryBO>();
		
		for (Category category : categoryList) {
			if (category == null)
				continue;
			CategoryBO categoryBO = createCategoryBO(category);
			categoryBOs.add(categoryBO);
		}
		
		if (categoryBOs != null && categoryBOs.size() > 0)
			Collections.sort(categoryBOs, new SortListUtils.CategoryBOComparator());
		return categoryBOs;
		
	}*/
	
	
	/*public static UserLogin createUserProfile(UserRequestBo userBO) {
		UserLogin userProfile = new UserLogin();
		//userProfile.setId(userBO.getId());
		userProfile.setUname(userBO.getUname());
		userProfile.setUpass(userBO.getUpass());
		System.out.println("MiscUtil userProfile="+userProfile);
		return userProfile;
	}*/

/*	public static UserRequestBo createUserProfileBO(UserLogin userProfile) {
		UserRequestBo userBO = new UserRequestBo();
		//userBO.setId(userProfile.getId());
		userBO.setUname(userProfile.getUname());
		userBO.setUpass(userProfile.getUpass());
		System.out.println("MiscUtil userBO="+userBO);
		return userBO;
	}*/

	/*private static CategoryBO createCategoryBO(Category category) {
		// TODO Auto-generated method stub
		return null;
	}
*/

	public static UserResponseBo createUserResponseBO(UserLogin userProfile) {
		if (userProfile == null)
			return null;
		UserResponseBo userResponseBO = new UserResponseBo();
		userResponseBO.setUname(userProfile.getUname());
		userResponseBO.setId(userProfile.getId());
		userResponseBO.setUpass(userProfile.getUpass());
		System.out.println("MiscUtil userResponseBO="+userResponseBO);
		return userResponseBO;
	}

	
	
	/**
	 * This method creates partners resource bean object
	 * {@link PartnersResources}.
	 * 
	 * @param userBO
	 *            (User bo object containing partners resource information
	 *            {@link UserRequestBO})
	 * @return (Returns partners resources object {@link PartnersResources})
	 */

}