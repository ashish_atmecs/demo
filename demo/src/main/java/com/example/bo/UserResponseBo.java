package com.example.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserResponseBo implements Serializable {
	private static final long serialVersionUID = -5325514466044513150L;
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("uname")
	private String uname;
	
	@JsonProperty("upass")
	private String upass;
	
	
	public UserResponseBo() {
		super();
	}
	
//	List<CategoryBO> categoryBOs;
	// List<PartnersProject> partnersProjects;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUname() {
		return uname;
	}
	
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public String getUpass() {
		return upass;
	}
	public void setUpass(String upass) {
		this.upass = upass;
	}
}
