package com.example.bo;

import java.io.Serializable;
import java.util.Date;


import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryBO implements Serializable{

	private static final long serialVersionUID = -6254149592474603325L;

	@JsonProperty("id")
	private int id;
	
	@NotNull
	@JsonProperty("category_name")
	private String categoryName;
	
	@NotNull
	@JsonProperty("category_description")
	private String categoryDescription;
	
	@JsonIgnore
	private Date creationTime;
	
	@JsonIgnore
	private Date modificationTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}
	
/*	@JsonProperty("partners_projects")
	private String partnersProjects;
*/	
	
	@Override
	public String toString() {
		return "CategoryBO [id=" + id + ", categoryName=" + categoryName + ", categoryDescription="
				+ categoryDescription + ", creationTime=" + creationTime + ", modificationTime=" + modificationTime + "]";
				
	}
		
	
}
