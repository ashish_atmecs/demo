package com.example.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.util.DateUtils;

@Entity
@Table(name = "category")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="category_name", nullable = false, unique = true, length = 200)
	private String categoryName;
	
	@Column(name="category_description", nullable = false, unique = true, length = 1000)
	private String categoryDescription;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_time", nullable = false)
	private Date creationTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modification_time", nullable = false)
	private Date modificationTime;
	/*
	@OneToMany(mappedBy = "category")
	private List<Offer> offers;
	*/
	
	public Category(){
		super();
	}

	@PrePersist
	public void prePersist() {
		this.setCreationTime(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
		this.setModificationTime(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}
	
	@PreUpdate
	public void preUpdate() {
		this.setModificationTime(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}
}