package com.example.controller;
import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.bo.BaseResponse;
import com.example.bo.UserRequestBo;
import com.example.bo.UserResponseBo;
import com.example.service.UserService;
import com.example.util.ResponseMessagesUtils;


/**
 * 
 * @author Ramnaresh.Mantri
 *
 */
@RestController
@RequestMapping("/users")

public class UserServices implements ResponseMessagesUtils{
	@Autowired
	private UserService userService;
	
	private static final Logger log = Logger.getLogger(UserServices.class);
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity loginUser(@RequestBody UserRequestBo userBO) {
		HttpStatus httpResponseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		String responseMessage = StringUtils.EMPTY;
		String status = StringUtils.EMPTY;
		BaseResponse baseResponse = new BaseResponse();
		try {
			System.out.println("From UserServices Class userBO="+userBO);
			log.info("logger is working into the project");
			System.out.println("Hello");
			
			UserResponseBo userResponseBO = userService.getUserLogin(userBO.getUname(), userBO.getUpass());
			if (userResponseBO != null) {
				httpResponseStatus = HttpStatus.OK;
				return new ResponseEntity<UserResponseBo>(userResponseBO, httpResponseStatus);
			} else {
				httpResponseStatus = HttpStatus.FORBIDDEN;
				responseMessage = "User does not exist.";
				status = SUCCESS; 
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseMessage = e.getMessage();
			status = ERROR;
		}
		baseResponse.setMessage(responseMessage);
		baseResponse.setStatus(status);
		baseResponse.setStatusCode(httpResponseStatus.value() + "");
		return new ResponseEntity<BaseResponse>(baseResponse, httpResponseStatus);
	}
	
}
