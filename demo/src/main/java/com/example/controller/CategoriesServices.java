package com.example.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.bo.BaseResponse;
import com.example.bo.CategoryBO;
import com.example.service.CategoriesService;



/**
 * A restful service containing various offer related to the category
 * @author Ramnaresh.Mantri
 *
 */
@RestController
@RequestMapping("/categories")
public class CategoriesServices {
	
		
	@Autowired
	private CategoriesService categoriesService;
	
	
	/**
	 * Restful service to register a user.
	 * 
	 * @param user
	 *            The user object that contains the JSON content.
	 * @return HTTP Response({@link javax.ws.rs.core.Response}) 200 if user
	 *         registered successfully. Else 417 stating the Expectation failed.
	 *         In case of Exception it is 500.
	 */
	@RequestMapping(value = "/createCategory", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> createCategory(@RequestBody CategoryBO categoryBO) {
		BaseResponse baseResponse = new BaseResponse();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String responseMessage = "";
		try {
			if (categoriesService.createCategory(categoryBO)) {
				status = HttpStatus.OK;
				responseMessage = "success";
				baseResponse.setMessage(responseMessage);
				baseResponse.setStatus("success");
			} else {
				status = HttpStatus.EXPECTATION_FAILED;
				responseMessage = "success";
				baseResponse.setMessage(responseMessage);
				baseResponse.setStatus("error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseMessage = e.getMessage();
		}
		return new ResponseEntity<BaseResponse>(baseResponse, status);
	}


}
